<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatabaseForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('products', function(Blueprint $table) {
        $table->foreign('categoryId')->references('id')->on('categories')
        ->onDelete('no action')
        ->onUpdate('no action');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('products', function(Blueprint $table) {
           $table->dropForeign('categories_categoryId_foreign');
        });
    }
}
