<?php

namespace App\Http\Controllers;
use Auth;
use App\ShoppingCart;
use App\CartItem;
use App\Order;
use App\OrderItem;
use App\ShippingInfo;
use Session;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }

    public function checkout(Request $request)
    {

      $request->validate([
         'fullName' => 'required|string|max:255',
         'phone' => 'required|numeric|digits_between:2,14',
         'city' => 'required|string|max:255',
         'address' => 'required|string'
      ]);
      // shipping detail
      $userId = Auth::user()->id;
      $shippingInfo = new ShippingInfo;
      $shippingInfo->userId = $userId;
      $shippingInfo->fullName = $request->fullName;
      $shippingInfo->phone = $request->phone;
      $shippingInfo->city = $request->city;
      $shippingInfo->address = $request->address;
      $shippingInfo->save();
      $shippingId = $shippingInfo->id;
      //order save

      $cart = ShoppingCart::where('userId','=',$userId)->first();
      if ($cart) {
          $items = CartItem::where('shoppingCartId' , '=' , $cart->id)->join('products','productId','=','products.id')
              ->select('cart_items.*','price', 'products.quantity as prodQuantity')->get();
          $price = 0;
          foreach ($items as $value) {
            $price += $value->quantity * $value->price;
          }
          $order = new Order;
          $userId = Auth::user()->id;
          $order->userId = $userId;
          $order->totalPrice = $price;
          $order->shippingId = $shippingId;
          $order->save();
          foreach($items as $item)
          {
            // dd($item);
            $productId = $item->productId;
            $quantity = $item->quantity;
            $price = $item->price;
            $orderItem = new orderItem;
            $orderItem->productId = $productId;
            $orderItem->orderId = $order->id;
            $orderItem->quantity = $quantity;
            $orderItem->save();
         }

          //for clearing Cart
          $userId = Auth::user()->id;
          $shoppingitems = ShoppingCart::where('userId','=',$userId)->get();
          if(count($shoppingitems) == 0)
          {
            Session::flash('status' , 'Cart is already cleared');
            return redirect(route('shoppingCart.index'));
          }
          foreach ($shoppingitems as $TempValue) {
             $items = CartItem::where('shoppingCartId','=',$TempValue->id)->get();
             if(count($items) > 0)
             {
               foreach($items as $item)
               {
                 $item->delete();
               }

             }
          }

         Session::flash('status', "Checkout Completed Successfully");
         return redirect(route('shoppingCart.index'));
      }else
      {
        Session::flash('status', "Cart is empty");
        return redirect(route('shoppingCart.index'));
      }

      // dd($cart);
      // if (!is_null($cart))
      // {
      //    $items = CartItem::where('shoppingCartId' , '=' , $cart->id)->get();
      //    if(count($items) > 0)
      //    {
      //      $order = new Order;
      //      $userId = Auth::user()->id;
      //      $order->userId = $userId;
      //      $order->save();
      //      foreach($items as $item)
      //      {
      //        $productId = $item['productId'];
      //        $quantity = $item['quantity'];
      //        $price = $item['price'];
      //        $orderItem = new orderItem;
      //        $orderItem->productId = $productId;
      //        $orderItem->orderId = $order->id;
      //        $orderItem->quantity = $quantity;
      //        $orderItem->save();
      //     }
      //     Session::flash('status', "Checkout Completed Successfully");
      //     return redirect(route('shoppingCart.index'));
      //   }
      //   else
      //   {
      //     Session::flash('status', "Cart is empty");
      //     return redirect(route('shoppingCart.index'));
      //   }
      // }
   }
   public function shipping()
   {
     // dd('here');
     return view('delivery');
   }
}
